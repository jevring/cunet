package cu.identd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.InetSocketAddress;
import java.util.HashMap;

public class IdentServer extends Thread {
    private final int port = 113;
    private final ServerSocket server;
    private boolean running = false;
    private final HashMap<String, String> idents = new HashMap<String, String>();

    public IdentServer() throws IOException {
        server = new ServerSocket(port, 50);
    }

    public void registerIdent(InetSocketAddress remoteHost, String ident) {
        idents.put(remoteHost.getAddress().getHostAddress() + ":" + remoteHost.getPort(), ident);
    }

    public void deRegisterIdent(InetSocketAddress remoteHost) {
        idents.remove(remoteHost.getAddress().getHostAddress() + ":" + remoteHost.getPort());
    }

    // http://www.faqs.org/rfcs/rfc1413.html
    public void run() {
        running = true;
        while (running) {
            try {
                Socket client = server.accept();
                PrintWriter out = new PrintWriter(client.getOutputStream());
                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                String line = in.readLine();
                String[] ports = line.split(",");
                // 32000, 23 (if connecting to telnet, and 32000 is an arbitrary "high" port on THIS system)
                String username = idents.get(client.getInetAddress().getHostAddress() + ":" + ports[1]);
                if (username != null) {
                    out.print(line + " : USERID : UNIX : " + username + "\r\n");
                } else {
                    out.print(line + " : ERROR : NO-USER\r\n");
                }
                out.flush();
                out.close();
                in.close();
                client.close();
            } catch (IOException e) {
                //e.printStackTrace();
            }
        }
    }

    public void terminate() {
        running = false;
        try {
            server.close();
        } catch (IOException e) {
            // it's ok, this should throw an exception, just drop it
        }
    }
}